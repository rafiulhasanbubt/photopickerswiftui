//
//  PhotoPickerSwiftUIApp.swift
//  PhotoPickerSwiftUI
//
//  Created by rafiul hasan on 9/1/22.
//

import SwiftUI

@main
struct PhotoPickerSwiftUIApp: App {
    @StateObject var vm = ViewModel()
    
    var body: some Scene {
        WindowGroup {
            ContentView().environmentObject(vm)
                .onAppear {
                    UserDefaults.standard.setValue(false, forKey: "_UIConstraintBasedLayoutLogUnsatisfiable")
                }
        }
    }
}
