//
//  ButtonLabelView.swift
//  PhotoPickerSwiftUI
//
//  Created by rafiul hasan on 9/1/22.
//

import SwiftUI

struct ButtonLabelView: View {
    let symbolName: String
    let label: String
    
    var body: some View {
        HStack {
            Image(systemName: symbolName)
            Text(label)
        }
        .font(.headline)
        .padding()
        .frame(height: 40)
        .background(Color.blue)
        .foregroundColor(.white)
        .cornerRadius(15)
    }
}

struct ButtonLabelView_Previews: PreviewProvider {
    static var previews: some View {
        ButtonLabelView(symbolName: "camera", label: "Camera")
    }
}
