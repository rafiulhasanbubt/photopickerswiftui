//
//  PickerButtonsView.swift
//  PhotoPickerSwiftUI
//
//  Created by rafiul hasan on 9/1/22.
//

import SwiftUI

struct PickerButtonsView: View {
    @EnvironmentObject var vm: ViewModel
    
    var body: some View {
        HStack {
            Button {
                vm.source = .camera
                vm.showPhotoPicker()
            } label: {
                ButtonLabelView(symbolName: "camera", label: "Camera")
            }
            .alert("Error", isPresented: $vm.showCameraAlert, presenting: vm.cameraError, actions: { cameraError in
                cameraError.button
            }, message: { cameraError in
                Text(cameraError.message)
            })
            Button {
                vm.source = .library
                vm.showPhotoPicker()
            } label: {
                ButtonLabelView(symbolName: "photo", label: "Photos")
            }
        }
    }
}

struct PickerButtonsView_Previews: PreviewProvider {
    static var previews: some View {
        PickerButtonsView()
    }
}
