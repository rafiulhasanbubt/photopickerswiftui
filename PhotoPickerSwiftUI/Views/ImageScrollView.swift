//
//  ImageScrollView.swift
//  PhotoPickerSwiftUI
//
//  Created by rafiul hasan on 9/1/22.
//

import SwiftUI

struct ImageScrollView: View {
    @EnvironmentObject var vm: ViewModel
    
    var body: some View {
        ScrollView(.horizontal, showsIndicators: false) {
            HStack(spacing: 10) {
                ForEach(vm.myImages) { myImage in
                    VStack {
                        Image(uiImage: myImage.image)
                            .resizable()
                            .scaledToFill()
                            .frame(width: 100, height: 100)
                            .clipShape(RoundedRectangle(cornerRadius: 15))
                            .shadow(color: .black.opacity(0.6), radius: 2, x: 2, y: 2)
                        Text(myImage.name)
                    }
                    .onTapGesture {
                        vm.display(myImage)
                    }
                }
            }
        }.padding(.horizontal)
    }
}

struct ImageScrollView_Previews: PreviewProvider {
    static var previews: some View {
        ImageScrollView()
    }
}
